package ru.volkova.tm.api.controller;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void showProjectByIndex();

    void showProjectByName();

    void removeProjectByIndex();

    void removeProjectByName();

    void removeProjectById();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void startProjectById();

    void startProjectByName();

    void startProjectByIndex();

    void finishProjectById();

    void finishProjectByName();

    void finishProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByName();

    void changeProjectStatusByIndex();

}
