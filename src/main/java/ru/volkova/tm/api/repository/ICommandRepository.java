package ru.volkova.tm.api.repository;

import ru.volkova.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
