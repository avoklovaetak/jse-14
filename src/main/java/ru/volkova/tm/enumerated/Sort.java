package ru.volkova.tm.enumerated;

import ru.volkova.tm.comparator.ComparatorByCreated;
import ru.volkova.tm.comparator.ComparatorByDateStart;
import ru.volkova.tm.comparator.ComparatorByName;
import ru.volkova.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    NAME("sort by name", ComparatorByName.getInstance()),
    CREATED("sort by created", ComparatorByCreated.getInstance()),
    DATE_START("sort by date start", ComparatorByDateStart.getInstance()),
    STATUS("sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.comparator = comparator;
        this.displayName = displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

}
