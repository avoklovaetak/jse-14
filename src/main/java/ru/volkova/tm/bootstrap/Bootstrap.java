package ru.volkova.tm.bootstrap;

import ru.volkova.tm.api.controller.ICommandController;
import ru.volkova.tm.api.controller.IProjectController;
import ru.volkova.tm.api.controller.IProjectTaskController;
import ru.volkova.tm.api.controller.ITaskController;
import ru.volkova.tm.api.repository.ICommandRepository;
import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.ICommandService;
import ru.volkova.tm.api.service.IProjectService;
import ru.volkova.tm.api.service.IProjectTaskService;
import ru.volkova.tm.api.service.ITaskService;
import ru.volkova.tm.constant.ArgumentConst;
import ru.volkova.tm.constant.TerminalConst;
import ru.volkova.tm.controller.CommandController;
import ru.volkova.tm.controller.ProjectController;
import ru.volkova.tm.controller.ProjectTaskController;
import ru.volkova.tm.controller.TaskController;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.repository.CommandRepository;
import ru.volkova.tm.repository.ProjectRepository;
import ru.volkova.tm.repository.TaskRepository;
import ru.volkova.tm.service.CommandService;
import ru.volkova.tm.service.ProjectService;
import ru.volkova.tm.service.ProjectTaskService;
import ru.volkova.tm.service.TaskService;
import ru.volkova.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void initData() {
        projectService.add("DEMO 1", "1").setStatus(Status.NOT_STARTED);
        projectService.add("DEMO 4", "1").setStatus(Status.NOT_STARTED);
    }

    public void run(final String... args){
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initData();
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg){
        if (arg == null) return;
        switch (arg){
            case ArgumentConst.ARG_ABOUT: commandController.showAbout(); break;
            case ArgumentConst.ARG_HELP: commandController.showHelp(); break;
            case ArgumentConst.ARG_VERSION: commandController.showVersion(); break;
            case ArgumentConst.ARG_INFO: commandController.showSystemInfo(); break;
            default: commandController.showIncorrectArgument();
        }
    }

    public void parseCommand(final String command){
        if (command == null) return;
        switch (command){
            case TerminalConst.CMD_ABOUT: commandController.showAbout(); break;
            case TerminalConst.CMD_HELP: commandController.showHelp(); break;
            case TerminalConst.CMD_VERSION: commandController.showVersion(); break;
            case TerminalConst.CMD_INFO: commandController.showSystemInfo(); break;
            case TerminalConst.CMD_COMMANDS: commandController.showCommands(); break;
            case TerminalConst.CMD_ARGUMENTS: commandController.showArguments(); break;
            case TerminalConst.CMD_EXIT: commandController.exit(); break;
            case TerminalConst.TASK_LIST: taskController.showList(); break;
            case TerminalConst.TASK_CREATE: taskController.create(); break;
            case TerminalConst.TASK_CLEAR: taskController.clear(); break;
            case TerminalConst.PROJECT_LIST: projectController.showList(); break;
            case TerminalConst.PROJECT_CREATE: projectController.create(); break;
            case TerminalConst.PROJECT_CLEAR: projectController.clear(); break;
            case TerminalConst.TASK_VIEW_BY_ID: taskController.showTaskById(); break;
            case TerminalConst.TASK_VIEW_BY_INDEX: taskController.showTaskByIndex(); break;
            case TerminalConst.TASK_VIEW_BY_NAME: taskController.showTaskByName(); break;
            case TerminalConst.TASK_REMOVE_BY_ID: taskController.removeTaskById(); break;
            case TerminalConst.TASK_REMOVE_BY_INDEX: taskController.removeTaskByIndex(); break;
            case TerminalConst.TASK_REMOVE_BY_NAME: taskController.removeTaskByName(); break;
            case TerminalConst.TASK_UPDATE_BY_ID: taskController.updateTaskById(); break;
            case TerminalConst.TASK_UPDATE_BY_INDEX: taskController.updateTaskByIndex(); break;
            case TerminalConst.TASK_START_BY_ID: taskController.startTaskById(); break;
            case TerminalConst.TASK_START_BY_NAME: taskController.startTaskByName(); break;
            case TerminalConst.TASK_START_BY_INDEX: taskController.startTaskByIndex(); break;
            case TerminalConst.TASK_FINISH_BY_ID: taskController.finishTaskById(); break;
            case TerminalConst.TASK_FINISH_BY_NAME: taskController.finishTaskByName(); break;
            case TerminalConst.TASK_FINISH_BY_INDEX: taskController.finishTaskByIndex(); break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID: taskController.changeTaskStatusById(); break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_NAME: taskController.changeTaskStatusByName(); break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX: taskController.changeTaskStatusByIndex(); break;
            case TerminalConst.PROJECT_VIEW_BY_ID: projectController.showProjectById(); break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX: projectController.showProjectByIndex(); break;
            case TerminalConst.PROJECT_VIEW_BY_NAME: projectController.showProjectByName(); break;
            case TerminalConst.PROJECT_REMOVE_BY_ID: projectController.removeProjectById(); break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX: projectController.removeProjectByIndex(); break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME: projectController.removeProjectByName(); break;
            case TerminalConst.PROJECT_UPDATE_BY_ID: projectController.updateProjectById(); break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX: projectController.updateProjectByIndex(); break;
            case TerminalConst.PROJECT_START_BY_ID: projectController.startProjectById(); break;
            case TerminalConst.PROJECT_START_BY_NAME: projectController.startProjectByName(); break;
            case TerminalConst.PROJECT_START_BY_INDEX: projectController.startProjectByIndex(); break;
            case TerminalConst.PROJECT_FINISH_BY_ID: projectController.finishProjectById(); break;
            case TerminalConst.PROJECT_FINISH_BY_NAME: projectController.finishProjectByName(); break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX: projectController.finishProjectByIndex(); break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID: projectController.changeProjectStatusById(); break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME: projectController.changeProjectStatusByName(); break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX: projectController.changeProjectStatusByIndex(); break;
            case TerminalConst.TASK_BIND_BY_PROJECT_ID: projectTaskController.bindTaskByProjectId(); break;
            case TerminalConst.TASK_FIND_ALL_BY_PROJECT_ID: projectTaskController.findAllTasksByProjectId(); break;
            case TerminalConst.TASK_UNBIND_BY_PROJECT_ID: projectTaskController.unbindTaskByProjectId(); break;
            case TerminalConst.PROJECT_REMOVE_BY_ID_CASCADE: projectTaskController.removeProjectById(); break;
            default: commandController.showIncorrectCommand();
        }
    }

    public boolean parseArgs(String[] args){
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
